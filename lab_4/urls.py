from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method index
    path('add-note', add_note, name='add_note'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method add_note
    path('note-list', note_list, name='note_list'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method note_list
    path('list', index, name='index'),
    # buat handler navbar
]
