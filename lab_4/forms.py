from django import forms
from .models import Note

# buat user bisa milih tanggal-bulan-tahun
class DateInput(forms.DateInput): 
    input_type = 'date'

class NoteForm (forms.ModelForm):
	class Meta:
		model = Note
		fields = "__all__"
		widgets = {
            'to': forms.TextInput(attrs={'placeholder': 'Enter recipient', 'class' : 'inputan d-flex flex-column w-100 p-2'}),
            'froms': forms.TextInput(attrs={'placeholder': 'Enter sender','class' : 'inputan d-flex flex-column w-100 p-2'}),
            'title': forms.TextInput(attrs={'placeholder': 'Enter title','class' : 'inputan d-flex flex-column w-100 p-2'}),
            'message': forms.TextInput(attrs={'placeholder': 'Enter message','class' : 'inputan d-flex flex-column w-100 p-2'})
        }
        # Set the fields attribute to the special value '__all__' 
        # to indicate that all fields in the model should be used.
	
