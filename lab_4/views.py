from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Note
from .forms import NoteForm

# def index(request):
#     note = Note.objects.all().values()
#     response = {'notes' : note}
#     return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def index(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, label_suffix="") # label_suffix="" -> ilangin titik-dua (:) untuk field
      
    # check if form data is valid
    if (form.is_valid and request.method == 'POST'):
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4')
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_note_list.html', response)