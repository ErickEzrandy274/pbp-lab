from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method index
    path('xml', xml , name='xml'),
    path('json', json , name='json')
]
