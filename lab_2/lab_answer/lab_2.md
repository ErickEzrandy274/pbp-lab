[1. Apakah perbedaan antara JSON dan XML?](https://www.monitorteknologi.com/perbedaan-json-dan-xml/)


    XML =>  - Bahasa markup (bukan bahasa pemrograman) yang memiliki tag untuk mendefinisikan elemen
            - Data XML disimpan sebagai tree structure
            - Dapat melakukan pemrosesan dan pemformatan dokumen dan objek
            - Besar dan lambat dalam penguraian sehingga transmisi data lebih lambat
            - Mendukung namespaces, komentar, dan metadata

    JSON => - Hanyalah format yang ditulis dalam JavaScript
            - Data disimpan seperti map dengan pasangan key-value
            - Tidak melakukan pemrosesan atau perhitungan apa pun
            - Sangat cepat karena ukuran file sangat kecil dan penguraian lebih cepat oleh mesin JavaScript sehingga transfer data lebih  cepat
            - Tidak ada ketentuan untuk namespace, menambahkan komentar, atau menulis metadata


[2. Apakah perbedaan antara HTML dan XML?](https://id.natapa.org/difference-between-xml-and-html-2446)
[source2](https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html)


    HTML => - Dirancang untuk menampilkan data dengan fokus bagaimana data itu terlihat
            - Digunakan untuk mendesain halaman web yang akan di-render di sisi klien
            - Memiliki tag yang telah ditentukan
            - Tag penutup bisa diberikan atau tidak (optional)
            - Nilai atribut dapat hadir tanpa tanda kutip
    
    XML =>  - Dirancang untuk mengangkut dan menyimpan data dengan fokus pada apa itu data.
            - Digunakan untuk mentransport data antara aplikasi dan database
            - Memiliki tag khusus yang dapat ditemukan atau ditentukan oleh programmer
            - Harus menggunakan tag penutup
            - Nilai atribut harus diapit di dalam tanda kutip