import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// source https://dev.to/aspiiire/easy-way-to-write-forms-in-flutter-37ni
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pedulilindungi2.0',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'Pedulilindungi2.0'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: const MyCustomForm(),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  String? dropDownValue = 'Jenis Kelamin';
  List<String> disabledItems = <String>['Jenis Kelamin'];
  List<String> items = <String>[
    'Jenis Kelamin',
    'Laki-laki',
    'Perempuan'
  ];
  DateTime selectedDate = DateTime.now();
  DateTime? updatedDt;
  TextEditingController dateCtl = TextEditingController();

  // sources https://medium.com/flutter-community/a-deep-dive-into-datepicker-in-flutter-37e84f7d8d6c
  // _selectDate(BuildContext context) async {
  //   final DateTime? picked = await showDatePicker(
  //     context: context,
  //     initialDate: selectedDate, // Refer step 1
  //     firstDate: DateTime(1900),
  //     lastDate: DateTime(2025),
  //   );

  //   if (picked != null && picked != selectedDate) {
  //     setState(() {
  //       selectedDate = picked;
  //       updatedDt = DateFormat("dd-MM-yyyy").format(selectedDate);
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: ListView(
        padding: const EdgeInsets.only(left: 25, right: 35),
        children: [
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(top: 30),
            child:
                const Text('BIODATA PESERTA', 
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold
                      )
                ),
          ),
          TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.person),
              hintText: 'Masukkan nama lengkap kamu',
              labelText: 'Nama Lengkap',
              hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
              labelStyle: TextStyle(fontSize: 20),
            ),
            style: const TextStyle(color: Colors.black, fontSize: 20),
          ),
          TextFormField(
            decoration: const InputDecoration(
                icon: Icon(Icons.credit_card_outlined),
                hintText: 'Masukkan NIK kamu',
                labelText: 'NIK',
                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                labelStyle: TextStyle(fontSize: 20)),
            maxLength: 16,
            style: const TextStyle(color: Colors.black, fontSize: 20),
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(
                icon: Icon(Icons.phone),
                hintText: 'Masukkan nomor handphone kamu',
                labelText: 'Nomor Handphone',
                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                labelStyle: TextStyle(fontSize: 20)),
            maxLength: 12,
            style: const TextStyle(color: Colors.black, fontSize: 20),
          ),
          TextFormField(
            // sources https://stackoverflow.com/questions/54127847/flutter-how-to-display-datepicker-when-textformfield-is-clicked
            onTap: () async {
              FocusScope.of(context).requestFocus(FocusNode());
              DateTime? picked = await showDatePicker(
                  context: context, 
                  initialDate: selectedDate,
                  firstDate:DateTime(1900),
                  lastDate: DateTime(2100)
              );
              if (picked != null && picked != selectedDate) {
                setState(() {
                  selectedDate = picked;
                });
              }
              dateCtl.text = DateFormat("dd-MM-yyyy").format(selectedDate).substring(0,10);
              // method toIso8601String
            },
            controller: dateCtl,
            decoration: const InputDecoration(
                icon: Icon(Icons.calendar_today),
                labelText: 'Tanggal Lahir',
                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                labelStyle: TextStyle(fontSize: 20)),
            style: const TextStyle(color: Colors.black, fontSize: 20),
            maxLength: 10,
          ),
          Padding(
              padding: const EdgeInsets.only(
                top: 6,
              ),
              child: InputDecorator(
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person_outline_outlined),
                  ),
                  child: DropdownButton<String>(
                    isDense: true, // reduce height of the button.
                    isExpanded: true,
                    style: const TextStyle(color: Colors.black, fontSize: 20),
                    underline: DropdownButtonHideUnderline(child: Container()),
                    value: dropDownValue,
                    onChanged: (String? value) {
                      if (!disabledItems.contains(value)) {
                        setState(() {
                          dropDownValue = value;
                        });
                      }
                    },
                    items: items.map<DropdownMenuItem<String>>((String gender) {
                      return DropdownMenuItem<String>(
                          value: gender,
                          child: Text(
                            gender,
                            style: TextStyle(
                                color: disabledItems.contains(gender)
                                    ? Colors.black54
                                    : null,
                                fontSize: 20),
                          ));
                    }).toList(),
                  ))),
          TextFormField(
            decoration: const InputDecoration(
                icon: Icon(Icons.location_on_outlined),
                hintText: 'Masukkan alamat tinggal kamu',
                labelText: 'Alamat',
                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                labelStyle: TextStyle(fontSize: 20)),
            style: const TextStyle(color: Colors.black, fontSize: 20),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 30, horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  width: 90,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: const Color(0xFFFF0000),
                      // maximumSize: MaterialStateProperty.all(const Size(50, 40)),
                    ),
                    onPressed: () {},
                    child: const Text('BATAL',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        )),
                  ),
                )
                ,
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  width: 90,
                  child:TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      backgroundColor: const Color(0xFF4A1FFF),
                      // maximumSize: MaterialStateProperty.all(const Size(50, 40)),
                    ),
                    onPressed: () {},
                    child: const Text('SUBMIT',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        )),
                  ),
                )
              ]
            )
          )
        ],
      ),
    );
  }
}
