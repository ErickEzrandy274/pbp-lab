from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method index
    path('friends', friend_list , name='friend_list')
    # ketika buka http://127.0.0.1:8000/friends/ akan dihandle oleh method friend_list
    # Add friends path using friend_list Views
]
