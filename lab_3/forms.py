# import form class from django
from django import forms
# import Friend from models.py
from .models import Friend

# buat user bisa milih tanggal-bulan-tahun
class DateInput(forms.DateInput): 
    input_type = 'date'

class FriendForm (forms.ModelForm):
	class Meta:
		model = Friend
		fields = "__all__"
		widgets = {
            'dob': DateInput(),
        }
        # Set the fields attribute to the special value '__all__' 
        # to indicate that all fields in the model should be used.
	
