from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    # print(Friend.objects.all().values());
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None)
      
    # check if form data is valid
    if (form.is_valid and request.method == 'POST'):
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-3')
  
    context['form']= form
    return render(request, "lab3_form.html", context)