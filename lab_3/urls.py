from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method index
    path('add', add_friend, name='add'),
    # ketika buka http://127.0.0.1:8000 akan dihandle oleh method add_friend
]
